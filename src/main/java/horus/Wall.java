package horus;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * A class representing a Wall that is an implementation of the Structure interface
 * along with an implementation of its methods.
 * It has one private field representing the list of blocks that make up the Wall.
 */
public class Wall implements Structure {

    private List<Block> blocks;

    /**
     * @param color
     * @return
     *
     * The method takes String color as a parameter and returns a Block of the specified color wrapped in Optional .
     * The function loops through all the elements of the list of blocks , first checks if the block is an
     * instance of CompositeBlock . If yes, the next loop among the elements that make up the CompositeBlock searches
     * for an element with the desired color using the checkColorOrMaterial function and returns this element wrapped in Optional .
     * If the block is not an instance of CompositeBlock for each element we check whether the element is not null
     * and using the checkColorOrMaterial function whether the element has the desired color , if both conditions are met
     * the found block is returned wrapped as Optional.
     * If no block is found an empty Optional is returned.
     */
    @Override
    public Optional<Block> findBlockByColor(final String color) {

        for (Block block : blocks) {
            if (block instanceof CompositeBlock) {
                CompositeBlock compositeBlocks = (CompositeBlock) block;
                for (Block compositeBlock : compositeBlocks.getBlocks()) {
                    if (checkColorOrMaterial(compositeBlock.getColor(), color)) {
                        return Optional.of(compositeBlock);
                    }
                }
            } else if (block != null && checkColorOrMaterial(block.getColor(), color)) {
                return Optional.of(block);
            }
        }
        return Optional.empty();
    }

    /**
     * @param material
     * @return
     *
     * The method takes String material as a parameter and returns a list of blocks created from the specified material.
     * At first, the function creates an empty list to which matching items will be added.
     * Then it loops through all the elements of the Wall object. The first step in the loop is to check if the element
     * is a CompositeBlock instance. If yes , the given block is projected onto the CompositeBlock object. Then another loop
     * goes through all the elements of the CompositeBlock searching for elements of the specified material and adding them to the list.
     * If the element is not a CompositeBlock instance the function checks the compatibility of the material in the block
     * and, if it is a match, adds it to the list.
     */
    @Override
    public List<Block> findBlocksByMaterial(final String material) {
        List<Block> materialBlocks = new ArrayList<>();

        for (Block block : blocks) {
            if (block instanceof CompositeBlock) {
                CompositeBlock compositeBlocks = (CompositeBlock) block;
                for (Block compositeBlock : compositeBlocks.getBlocks()) {
                    if (checkColorOrMaterial(compositeBlock.getMaterial(), material)) {
                        materialBlocks.add(compositeBlock);
                    }
                }
            } else if (checkColorOrMaterial(block.getMaterial(), material)) {
                materialBlocks.add(block);
            }
        }
        return materialBlocks;
    }

    /**
     * @return
     *
     * A method that counts the number of elements that make up the entire Wall structure.
     * At the beginning, a counter is created which is an integer and is initialized with the value 0.
     * The function loops through all the elements of the Wall , if the block is a CompositeBlock instance another loop is started which goes through all the elements of the CompositeBlock incrementing the counter at each element.Otherwise the counter is incremented at each element.
     * The value returned is the number of elements of the entire structure.
     */
    @Override
    public int count() {
        int counter = 0;
        for (Block block : blocks) {
            if (block instanceof CompositeBlock) {
                CompositeBlock compositeBlocks = (CompositeBlock) block;
                for (Block compositeBlock : compositeBlocks.getBlocks()) {
                    counter++;
                }
            } else {
                counter++;
            }

        }
        return counter;
    }

    /**
     * @param blockToCheck
     * @param colorOrMaterial
     * @return
     *
     * A private helper method for comparing the color or material
     * of a block with the color or material of a function parameter.
     * Returns boolean value , true if both strings are the same.
     */
    private boolean checkColorOrMaterial(String blockToCheck, String colorOrMaterial) {
        return blockToCheck.equals(colorOrMaterial);
    }

    /**
     * @param blocks
     *
     * Setter method created for unit test.
     */
    void setBlocks(final List<Block> blocks) {
        this.blocks = blocks;
    }

}
