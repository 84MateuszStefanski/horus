package horus;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WallTest {

    @Test
    public void shouldFindBlockByColor() {
        //given
        String color = "red";
        Wall wall = generateWall();

        //when
        Optional<Block> optionalBlock = wall.findBlockByColor(color);

        //then
        assertEquals("red", optionalBlock.get().getColor());
        assertEquals("stone", optionalBlock.get().getMaterial());
    }

    @Test
    public void shouldFindCompositeBlockByColor() {
        //given
        String color = "white";
        Wall wall = generateWall();

        //when
        Optional<Block> optionalBlock = wall.findBlockByColor(color);

        //then
        assertEquals("white", optionalBlock.get().getColor());
        assertEquals("sand", optionalBlock.get().getMaterial());
    }

    @Test
    public void shouldFindBlocksByMaterial() {
        //given
        String material = "stone";
        Wall wall = generateWall();

        //when
        List<Block> blocks = wall.findBlocksByMaterial(material);

        //then
        assertEquals(4, blocks.size());
    }

    public void shouldFindCompositeBlocksByMaterial() {
        //given
        String material = "sand";
        Wall wall = generateWall();

        //when
        List<Block> blocks = wall.findBlocksByMaterial(material);

        //then
        assertEquals(1, blocks.size());
    }

    @Test
    public void shouldCountAllBlocksInWall() {
        //given
        Wall wall = generateWall();

        //when
        int count = wall.count();

        //then
        assertEquals(7, count);
    }

    private Wall generateWall() {
        List<Block> blocks = new ArrayList<>();
        Wall wall = new Wall();

        Block block1 = new Block() {
            @Override
            public String getColor() {
                return "red";
            }

            @Override
            public String getMaterial() {
                return "stone";
            }
        };

        Block block2 = new Block() {
            @Override
            public String getColor() {
                return "green";
            }

            @Override
            public String getMaterial() {
                return "stone";
            }
        };

        Block block3 = new Block() {
            @Override
            public String getColor() {
                return "black";
            }

            @Override
            public String getMaterial() {
                return "marble";
            }
        };

        Block block5 = new Block() {
            @Override
            public String getColor() {
                return "white";
            }

            @Override
            public String getMaterial() {
                return "sand";
            }
        };

        Block block4 = new CompositeBlock() {

            @Override
            public List<Block> getBlocks() {
                return Arrays.asList(block1, block2, block3, block5);
            }

            @Override
            public String getColor() {
                return null;
            }

            @Override
            public String getMaterial() {
                return null;
            }
        };

        blocks.add(block1);
        blocks.add(block2);
        blocks.add(block3);
        blocks.add(block4);

        wall.setBlocks(blocks);
        return wall;
    }
}
